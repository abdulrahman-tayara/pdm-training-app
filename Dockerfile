FROM 232927913141.dkr.ecr.us-west-2.amazonaws.com/executor-base:v1.0

RUN env

RUN apt-get update

RUN apt-get install -y git

WORKDIR /root

ARG BITBUCKET_USER
ARG BITBUCKET_PASSWORD
ARG PIP_TOKEN
ENV BITBUCKET_PASSWORD $BITBUCKET_PASSWORD
ENV PIP_TOKEN $PIP_TOKEN
ENV BITBUCKET_USER $BITBUCKET_USER

RUN git clone https://"${BITBUCKET_USER}":"${BITBUCKET_PASSWORD}"@bitbucket.org/averroes_ai/executor-new.git

WORKDIR /root/executor-new

COPY . app

RUN ./prepare.sh

RUN python3 src/build.py app/notebook.ipynb

RUN pip install git+https://"${BITBUCKET_USER}":"${BITBUCKET_PASSWORD}"@bitbucket.org/averroes_ai/averroesutils.git

CMD ["python3", "src/execute.py", "app/notebook.ipynb"]